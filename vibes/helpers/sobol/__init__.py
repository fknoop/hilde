""" Module to provide quasi random numbers for efficient sampling """
# flake8: noqa

from .sobol import RandomState
