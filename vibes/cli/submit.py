"""`vibes run` part of the CLI"""

import click

from .misc import AliasedGroup, complete_files

paths = complete_files
_prefix = "vibes.submit"
_command = lambda c, s: f"vibes run {c} {s}"


def _start(settings_file, name, dry=False):
    """check if settings contain [slurm] and submit"""
    from vibes.settings import Settings
    from vibes.slurm.submit import submit as _submit

    settings = Settings(settings_file=settings_file)
    if "slurm" not in settings:
        raise click.ClickException(f"[slurm] settings not found in {settings_file}")

    dct = settings["slurm"]
    dct["name"] = name

    _submit(dct, command=_command(name, settings_file), dry=dry)


@click.command(cls=AliasedGroup)
@click.option("--dry", is_flag=True)
@click.pass_obj
def submit(obj, dry):
    """submit a vibes workflow to slurm"""
    obj.dry = dry


@submit.command("aims")
@click.argument("settings", default="aims.in", type=paths)
@click.pass_obj
def aims_submit(obj, settings):
    """submit one or several aims calculations from SETTINGS (default: aims.in)"""

    _start(settings, "aims", dry=obj.dry)


@submit.command("phonopy")
@click.argument("settings", default="phonopy.in", type=paths)
@click.pass_obj
def phonopy_run(obj, settings):
    """submit a phonopy calculation for SETTINGS (default: phonopy.in)"""

    _start(settings, "phonopy", dry=obj.dry)


@submit.command("phono3py")
@click.argument("settings", default="phono3py.in", type=paths)
@click.pass_obj
def phono3py_run(obj, settings):
    """submit a phonopy calculation for SETTINGS (default: phonopy.in)"""

    _start(settings, "phono3py", dry=obj.dry)


@submit.command("md")
@click.argument("settings", default="md.in", type=paths)
@click.pass_obj
def md_run(obj, settings):
    """submit an MD simulation from SETTINS (default: md.in)"""

    _start(settings, "md", dry=obj.dry)


@submit.command("relaxation")
@click.argument("settings", default="relaxation.in", type=paths)
@click.pass_obj
def relaxation_run(obj, settings):
    """submit an relaxation from SETTINS (default: relaxation.in)"""

    _start(settings, "relaxation", dry=obj.dry)
