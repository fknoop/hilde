References
===

## Work that was performed using `FHI-vibes`
- [Knoop, Purcell, Scheffler, Carbogno: _Anharmonicity Quantification for Materials_, in preparation]()

## Related Projets

- The [Atomic Simulation Environment](https://wiki.fysik.dtu.dk/ase/)
- [Phonopy](https://atztogo.github.io/phonopy/) and [Phono3py](https://atztogo.github.io/phono3py/)
- [**hiPhive** — High-order force constants for the masses](https://hiphive.materialsmodeling.org/index.html)
- [FHI-aims: FHI _ab initio_ molecular simulations](https://aimsclub.fhi-berlin.mpg.de/)
- [GIMS: Graphical Interface for Materials Simulations](http://gims.ms1p.org/)

