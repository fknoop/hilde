# References

1. <a name="Baroni2001"></a> S. Baroni, S. de Gironcoli, and A. Dal Corso, Rev. Mod. Phys. **73**, 515 (2001).
2. <a name="AshcroftMermin"></a> N. W. Ashcroft and N. D. Mermin, _Solid State Physics_, Saunders College Publishing (1976).
3. <a name="BornHuang"></a>  M. Born, and K. Huang, _Dynamical Theory of Crystal Lattices_, Oxford University Press (1962).
4. <a name="Togo2015"></a> [Atsushi Togo and Isao Tanaka, Scr. Mater., **108**, 1-5 (2015)](https://phonopy.github.io/phonopy/)
5. <a name="Parlinski1997"></a> K. Parlinski, Z. Q. Li, and Y. Kawazoe, Phys. Rev. Lett. **78**, 4063 (1997).
