Tutorial
===

In this tutorial, we introduce some of the functionality of `FHI-vibes` with hands-on examples.

!!! warning
	We assume that you are familiar with running *FHI-aims* calculations, and that you have [installed](../README.md#installation) and [configured](../README.md#configuration) `FHI-vibes` successfully.