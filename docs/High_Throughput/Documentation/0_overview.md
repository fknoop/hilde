This section defines the high-throughput definitions for all the keywords for the following tasks:
- [Installing FireWorks Dependency](../../Installation/0_setup)
- [Setting up a high-throughput workflow](../1_general_high_throughput)
- [qadapters](../2_qadapter)
- [K-Point Density Convergence](../3_optimize_k_grid)
- [Relaxation](../4_relaxation)
- [Phonon Calculations](../5_phonons)
- [Monte Carlo Sampling](../6_statistical_sampling)
- [Molecular Dynamics](../7_md)